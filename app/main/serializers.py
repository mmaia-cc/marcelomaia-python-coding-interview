from rest_framework import serializers
from ..users.models import User

class UsersSerializer(serializers.ModelSerializer):

    age = serializers.CharField()

    class Meta:
        model = User
        fields = '__all__'

