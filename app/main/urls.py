from django.urls import include, path
from rest_framework import routers

from .viewsets import UserViewSet

router = routers.DefaultRouter()

router.register('users', UserViewSet, basename='users blabla')

urlpatterns = [
    path('', include(router.urls))
]
