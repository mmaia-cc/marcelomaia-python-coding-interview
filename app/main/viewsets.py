from rest_framework.viewsets import ModelViewSet
from ..users.models import User
from .serializers import UsersSerializer

class UserViewSet(ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UsersSerializer
